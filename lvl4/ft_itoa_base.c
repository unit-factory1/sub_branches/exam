/*
Assignment name  : ft_itoa_base
Expected files   : ft_itoa_base.c
Allowed functions: malloc
--------------------------------------------------------------------------------

Write a function that converts an integer value to a null-terminated string
using the specified base and stores the result in a char array that you must
allocate.

The base is expressed as an integer, from 2 to 16. The characters comprising
the base are the digits from 0 to 9, followed by uppercase letter from A to F.

For example, base 4 would be "0123" and base 16 "0123456789ABCDEF".

If base is 10 and value is negative, the resulting string is preceded with a
minus sign (-). With any other base, value is always considered unsigned.

Your function must be declared as follows:

char	*ft_itoa_base(int value, int base);
--------------------------------------------------------------------------------
*/

#include <stdlib.h>

char	*itoa_base(int value, int base)
{
	int		nb;
	char 	*hex;
	char	*arr;
	int		i;

	i = 1;
	nb = value;
	hex = "0123456789ABCDEF";
	while (nb /= base)
		i++;
	if (value < 0 && base == 10)
		i++;
	arr = (char *)malloc(sizeof(char) * (i + 1));
	if (value == 0)
		arr[0] = 48;
	if (value < 0 && base == 10)
		arr[0] = '-';
	while (value)
	{
		arr[--i] = hex[value < 0 ? -(value % base) : value % base];
		value /= base;
	}
	return (arr);
}

#include <stdio.h>

int		main()
{
	printf("%s\n", itoa_base(69, 8));
	return (0);
}
